package com.cred.app.v1.core.rule.mysql.source;

import com.cred.app.PipelineConfigs;
import io.gitbub.devlibx.easy.helper.file.FileHelper;
import io.gitbub.devlibx.easy.helper.json.JsonUtils;
import org.junit.jupiter.api.Test;

public class SourceMySQLRulesTest {

    @Test
    public void testA() {
        String content = FileHelper.read("source_mysql_example.json");
        PipelineConfigs mysql = JsonUtils.readObject(content, PipelineConfigs.class);

        Processor rules = new Processor();
        rules.execute(mysql);
    }
}