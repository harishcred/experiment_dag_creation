package com.cred.app;

import com.cred.app.v1.core.rule.mysql.source.Processor;
import io.gitbub.devlibx.easy.helper.file.FileHelper;
import io.gitbub.devlibx.easy.helper.json.JsonUtils;

public class App {
    public static void main(String[] args) throws Exception {
        String content = FileHelper.read("source_mysql_example.json");
        PipelineConfigs mysql = JsonUtils.readObject(content, PipelineConfigs.class);

        Processor rules = new Processor();
        rules.execute(mysql);

    }
}


