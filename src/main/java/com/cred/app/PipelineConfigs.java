package com.cred.app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.gitbub.devlibx.easy.helper.map.StringObjectMap;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PipelineConfigs {
    private SourceMySQLInputConfig.Mysql mysql;
    private StringObjectMap s3;
    private StringObjectMap redshift;

    public boolean hasMySQL() {
        return mysql != null;
    }
}
