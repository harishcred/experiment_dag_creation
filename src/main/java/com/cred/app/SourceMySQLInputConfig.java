package com.cred.app;

import lombok.Data;

import java.util.List;

public interface SourceMySQLInputConfig {
    @Data
    class ContextVariable {
    }

    @Data
    class Properties {
        public String memory;
        public String connection_property;
    }

    @Data
    class Definition {
        public String name;
        public String dataType;
    }

    @Data
    class Schema {
        public String type;
        public List<Definition> definition;
    }

    @Data
    class Metric {
        public String metrics_type;
        public String metrics_query;
    }

    @Data
    class Mysql {
        public String con_id;
        public String query;
        public String mode;
        public String context_query;
        public ContextVariable context_variable;
        public Properties properties;
        public Schema schema;
        public List<Metric> metrics;
        public List<String> rules;
    }

    @Data
    class Root {
        public Mysql mysql;
    }
}
