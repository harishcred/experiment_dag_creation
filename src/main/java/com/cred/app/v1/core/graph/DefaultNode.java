package com.cred.app.v1.core.graph;

import lombok.Builder;

import java.util.Objects;

@Builder
public class DefaultNode implements INode {
    private final String id;
    private final Object data;
    private final String type;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultNode node = (DefaultNode) o;
        return Objects.equals(id, node.id) && Objects.equals(type, node.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}
