package com.cred.app.v1.core.rule.mysql.source;

import com.cred.app.PipelineConfigs;
import com.cred.app.v1.core.graph.DefaultEdge;
import com.cred.app.v1.core.graph.DefaultNode;
import com.cred.app.v1.core.graph.IEdge;
import com.cred.app.v1.core.graph.INode;
import com.google.common.base.Strings;
import com.google.common.graph.MutableNetwork;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

import static com.cred.app.v1.core.graph.INode.SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY;
import static com.cred.app.v1.core.graph.INode.SOURCE_MYSQL__NODE__EXTRACT_CONTEXT_FROM_DB;

@SuppressWarnings("UnstableApiUsage")
@Rule(name = "mysql_source__has_context_query", description = "check if we have a context query", priority = 1)
public class HasContextQueryRule {

    @Condition
    public boolean hasContextQuery(@Fact("config") PipelineConfigs config) {
        return config.hasMySQL() && !Strings.isNullOrEmpty(config.getMysql().context_query);
    }

    @Action
    public void processContextQuery(
            @Fact("id") String id,
            @Fact("config") PipelineConfigs config,
            @Fact("graph") MutableNetwork<INode, IEdge> graph
    ) {
        // We need 3 tasks here
        // 1. Run the context get query from DB
        // 2. Run user context query

        DefaultNode getContextFromDb = DefaultNode.builder()
                .id(SOURCE_MYSQL__NODE__EXTRACT_CONTEXT_FROM_DB + "__" + id)
                .type(SOURCE_MYSQL__NODE__EXTRACT_CONTEXT_FROM_DB)
                .data(config.getMysql())
                .build();

        DefaultNode runContextQuery = DefaultNode.builder()
                .id(SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY + "__" + id)
                .type(SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY)
                .data(config.getMysql())
                .build();

        INode start = DefaultNode.builder().id("start").type("").build();
        graph.addEdge(start, getContextFromDb, DefaultEdge.builder().from(start).to(getContextFromDb).build());

        graph.addNode(getContextFromDb);
        graph.addNode(runContextQuery);
        graph.addEdge(getContextFromDb, runContextQuery, DefaultEdge.builder().from(getContextFromDb).to(runContextQuery).build());
    }
}
