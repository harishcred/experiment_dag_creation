package com.cred.app.v1.core.rule.mysql.source;

import com.cred.app.PipelineConfigs;
import com.cred.app.v1.core.graph.IEdge;
import com.cred.app.v1.core.graph.INode;
import com.google.common.graph.MutableNetwork;
import com.google.common.graph.NetworkBuilder;
import edu.uci.ics.jung.samples.DrawGraph;
import io.gitbub.devlibx.easy.helper.map.StringObjectMap;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;

import java.util.List;
import java.util.UUID;

@SuppressWarnings("UnstableApiUsage")
public class Processor {
    private final Rules rules;
    private final RulesEngine rulesEngine;

    public Processor() {
        rulesEngine = new DefaultRulesEngine();
        rules = new Rules();
        rules.register(new SourceMySqlRule());
        rules.register(new HasContextQueryRule());
        rules.register(new SelectQueryRule());
    }

    public List<StringObjectMap> execute(PipelineConfigs root) {
        Facts facts = new Facts();
        facts.put("id", UUID.randomUUID().toString());
        facts.put("config", root);
        MutableNetwork<INode, IEdge> graph = NetworkBuilder.directed().build();
        facts.put("graph", graph);

        rulesEngine.fire(rules, facts);
        System.out.println(facts.get("graph").toString());

        DrawGraph<INode, IEdge> drawGraph = new DrawGraph<>();
        drawGraph.draw(graph, (DrawGraph.IExtractor<INode, IEdge>) node -> node.getId());

        return facts.get("tasks");
    }
}
