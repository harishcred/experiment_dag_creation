package com.cred.app.v1.core.rule.mysql.source;

import com.cred.app.PipelineConfigs;
import com.cred.app.v1.core.graph.DefaultNode;
import com.cred.app.v1.core.graph.IEdge;
import com.cred.app.v1.core.graph.INode;
import com.google.common.graph.MutableNetwork;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

@SuppressWarnings("UnstableApiUsage")
@Rule(name = "mysql_source__is_mysql_source", description = "check if it is a mysql source", priority = 0)
public class SourceMySqlRule {

    @Condition
    public boolean isSourceMySQL(@Fact("config") PipelineConfigs config) {
        return config.hasMySQL();
    }

    @Action(order = 1)
    public void processContextQuery(
            @Fact("id") String id,
            @Fact("config") PipelineConfigs config,
            @Fact("graph") MutableNetwork<INode, IEdge> graph
    ) {
        DefaultNode start = DefaultNode.builder().id("start").type("").build();
        graph.addNode(start);
    }
}
