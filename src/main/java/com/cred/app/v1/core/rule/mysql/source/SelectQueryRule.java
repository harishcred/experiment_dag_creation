package com.cred.app.v1.core.rule.mysql.source;

import com.cred.app.PipelineConfigs;
import com.cred.app.v1.core.graph.DefaultEdge;
import com.cred.app.v1.core.graph.DefaultNode;
import com.cred.app.v1.core.graph.IEdge;
import com.cred.app.v1.core.graph.INode;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.graph.MutableNetwork;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

import static com.cred.app.v1.core.graph.INode.SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY;
import static com.cred.app.v1.core.graph.INode.SOURCE_MYSQL__NODE__EXECUTE_QUERY;

@SuppressWarnings("UnstableApiUsage")
@Rule(name = "mysql_source__select_query", description = "check if we have a select query", priority = 2)
public class SelectQueryRule {

    @Condition
    public boolean hasContextQuery(@Fact("config") PipelineConfigs config) {
        return config.hasMySQL() && !Strings.isNullOrEmpty(config.getMysql().query);
    }

    @Action
    public void processContextQuery(@Fact("id") String id,
                                    @Fact("config") PipelineConfigs config,
                                    @Fact("graph") MutableNetwork<INode, IEdge> graph
    ) {

        // We will do 1 thing here - we will run the user query

        DefaultNode runQuery = DefaultNode.builder()
                .id(SOURCE_MYSQL__NODE__EXECUTE_QUERY + "__" + id)
                .type(SOURCE_MYSQL__NODE__EXECUTE_QUERY)
                .data(config.getMysql())
                .build();
        graph.addNode(runQuery);

        DefaultNode end = DefaultNode.builder().id("end").type("").build();
        graph.addNode(end);

        graph.addEdge(runQuery, end, DefaultEdge.builder().from(end).to(runQuery).build());

        // TODO - If we do not have a context query then we will connect this node to start.
        // For now I connected it to context query execution node.

        DefaultNode runContextQuery = DefaultNode.builder()
                .id(SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY + "__" + id)
                .type(SOURCE_MYSQL__NODE__EXECUTE_CONTEXT_QUERY)
                .build();

        graph.nodes().stream().filter(node -> Objects.equal(node, runContextQuery)).findAny().ifPresent(node -> {
            graph.addEdge(node, runQuery, DefaultEdge.builder().from(node).to(runQuery).build());
        });


    }
}
