package com.cred.app.v1.core.graph;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.Objects;

@Builder
public class DefaultEdge implements IEdge {
    private final INode from;
    private final INode to;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultEdge edge = (DefaultEdge) o;
        return Objects.equals(from, edge.from) && Objects.equals(to, edge.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }
}
