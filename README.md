## Example of how a graph can be created from MySQL source

Run```javacom.cred.app.App``` to launch app. It will open-up a window to show the graph as well (for visualization)

### How it is implemented

Every source/sink type will have ```com.cred.app.v1.core.rule.mysql.source.Processor```. In this case it is for MySQL
source.

We can implement N rules to populate which is the output graph of this node. Then we can finally combine all the graphs
to build the final execution graph.

Note it is still a logical execution graph. We will build physical graph out of this (with airflow plugin).
